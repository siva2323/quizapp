import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

export const fetchQuizData = createAsyncThunk("features/quizSlice", async ({ categoriesDetails }) => {
    try {
        const quizDataRaw = await fetch(`https://opentdb.com/api.php?amount=10&category=${categoriesDetails.categoryNumber}&difficulty=${categoriesDetails.difficultyLevel}&type=multiple`);
        const quizDataConverted = await quizDataRaw.json();
        return quizDataConverted;
    } catch (error) {
        return console.log("Error occured while fetching data " + error);
    }
}
);

const quizSlice = createSlice({
    name: "quiz",
    initialState: {
        quizData: {},
        isPageLoading: false,
        categoryNumber: 0,
        categoryIndex: 0,
        categoryName: "",
        difficultyLevel: "initial",
        userAnswer: [],
        categoryNumbers: [15, 16, 17, 18, 19, 20],
        categoryNames: [
            "Video Games",
            "Board Games",
            "Nature",
            "Computers",
            "Mathematics",
            "Mythology"
        ]
    },
    reducers: {
        saveCategoryNumber: (state, action) => {
            state.categoryNumber = action.payload;
        },
        saveDifficultyLevel: (state, action) => {
            state.difficultyLevel = action.payload;
        },
        saveUserAnswer: (state, action) => {
            state.userAnswer = [...(state.userAnswer), action.payload];
        },
        saveCategoryIndex: (state, action) => {
            state.categoryIndex = action.payload;
        }, saveCategoryName: (state, action) => {
            state.categoryName = action.payload;
        }
    },
    extraReducers: {
        [fetchQuizData.pending]: (state) => {
            state.isPageLoading = true;
        },
        [fetchQuizData.fulfilled]: (state, action) => {
            state.isPageLoading = true;
            state.quizData = action.payload;
            state.isPageLoading = false;
        },
        [fetchQuizData.rejected]: (state) => {
            state.isPageLoading = false;
        }
    }
})

export const { saveCategoryNumber, saveDifficultyLevel, saveCategoryName, saveUserAnswer, saveCategoryIndex } = quizSlice.actions;
export default quizSlice.reducer;