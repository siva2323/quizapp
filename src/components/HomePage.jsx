import React from 'react'
import DisplayingCategories from './DisplayingCategories'
import { useSelector } from 'react-redux'

const QuizHomePage = () => {

    const categoryNumbers = useSelector(state => state.quizStore.categoryNumbers);

    return (
        <>
            <nav className="navbar navbar-expand-md bg-primary d-flex justify-content-center" >
                <h1 className='text-light'>Quiz Application</h1>
            </nav>
            <h2 className='text-center p-3'>Test your Skills Here</h2>
            <div className='container d-flex mt-3 flex-wrap justify-content-center' >
                {categoryNumbers?.map((everyCategoryNumber, index) => <DisplayingCategories index={index} />)}
            </div>
        </>
    )
}

export default QuizHomePage