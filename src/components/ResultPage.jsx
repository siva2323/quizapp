import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'


const ResultPage = ({ singleQuestion, singleAnswer }) => {

    const quizStore = useSelector(state => state.quizStore)
    let totalScore = 0;

    return (
        <div>
            <nav className="navbar navbar-expand-md bg-primary d-flex justify-content-between" >
                <Link to="/"><h3 className='text-light mx-4'>HOME</h3> </Link>
                <h1 className='text-light'>Result</h1>
                <h3 className='text-light mx-4'>✸</h3>
            </nav>
            <h2 className='text-center p-3'>Quiz Category : {quizStore.categoryNames[quizStore.categoryIndex]}</h2>
            <h2 className='text-center p-3'>Quiz Difficulty Level : {quizStore.difficultyLevel}</h2>
            <h2 className='text-center text-primary p-3'>Result</h2>
            <div className='table-responsive mx-5' >
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Question</th>
                            <th scope="col">Answer</th>
                            <th scope="col">Your Answer</th>
                            <th scope="col">Points</th>
                        </tr>
                    </thead>
                    <tbody>
                        {quizStore.userAnswer && quizStore.userAnswer?.map((everyAnswer, index) => {
                            if (everyAnswer === singleAnswer[index]) {
                                totalScore++;
                            }
                            return (
                                <tr key={index}>
                                    <th scope="row">{singleQuestion[index]}</th>
                                    <td>{singleAnswer[index]}</td>
                                    <td>{everyAnswer}</td>
                                    <td>{everyAnswer && singleAnswer[index] === everyAnswer ? "✅" : "❌"}</td>
                                    <td></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
            <h2 className='text-center'>Total Score : {totalScore}</h2>
        </div>
    )
}

export default ResultPage