import React from 'react'
import { Link } from 'react-router-dom';
import '../index.css'
import { useSelector } from 'react-redux';
import { saveCategoryIndex, saveCategoryName } from '../features/quizSlice';
import { useDispatch } from 'react-redux';

const DisplayingCategories = ({ index }) => {

    const categoryNames = useSelector(state => state.quizStore.categoryNames)
    const dispatch = useDispatch()

    dispatch(saveCategoryName("sj"));

    return (
        <div className="card m-3 text-center shadow-lg p-3 mb-5 bg-body rounded displayingCategoriesDiv" key={index + categoryNames[index]} >
            <img className="card-img-top" src={`${index}.jpg`} alt="Card image cap" />
            <div className="card-body">
                <h5 className="card-title">{categoryNames[index]}</h5>
                <p className="card-text"></p>
                {`This quiz will test you the knowledge of ${categoryNames[index]}`}

                <Link to={`${categoryNames[index].replaceAll(" ", "")}`} >
                    <button className="btn  btn-primary w-100" onClick={() => dispatch(saveCategoryIndex(index))}>
                        Take Quiz
                    </button>
                </Link>

            </div>
        </div>
    )
}

export default DisplayingCategories;