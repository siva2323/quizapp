import React from 'react'
import { Link } from 'react-router-dom'

const Spinner = () => {
    return (
        <>
            <nav className="navbar navbar-expand-md bg-primary d-flex justify-content-between" >
                <Link to="/"><h3 className='text-light mx-4'>HOME</h3> </Link>
                <h1 className='text-light'>Loading...</h1>
                <h2 className='text-light mx-4'>✸</h2>
            </nav>
            <div className='container d-flex flex-wrap justify-content-center'>
                <div className='mt-5'>
                    <h2>LOADING...</h2>
                    <div className="spinner-border m-5" role="status">
                    </div>
                </div>
            </div>
        </>
    )
}

export default Spinner