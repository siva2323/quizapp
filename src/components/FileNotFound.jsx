import React from 'react'
import { Link } from 'react-router-dom';

const FileNotFound = () => {
    return (
        <div>
            <nav className="navbar navbar-expand-md bg-primary d-flex justify-content-between" >
                <Link to="/"><h3 className='text-light mx-3'>HOME</h3> </Link>
                <h1 className='text-light'>File Not Found</h1>
                <h2 className='text-light mx-3'>✸</h2>
            </nav>
            <h2 className='text-center m-5'>File Not Found</h2>
            <Link to="/">
                <button className='mx-5 p-3 bg-primary text-light'>Go Home</button>
            </Link>
        </div>
    )
}

export default FileNotFound;