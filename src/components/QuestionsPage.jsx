import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react';
import { fetchQuizData, saveUserAnswer } from '../features/quizSlice';
import { Link } from 'react-router-dom';
import ResultPage from './ResultPage';
import Spinner from './Spinner';
import NotFound from './NotFound';
import FileNotFound from './FileNotFound';

const QuestionsPage = () => {

    const categoryNumber = useSelector(state => state.quizStore.categoryNumber)
    const difficultyLevel = useSelector(state => state.quizStore.difficultyLevel)
    const categoriesDetails = { categoryNumber: categoryNumber, difficultyLevel: difficultyLevel };
    const dispatch = useDispatch();
    const [questionCount, setQuestionCount] = useState(0);
    const [isSelected, setSelected] = useState(false);
    const [currentWord, setCurrentWord] = useState("")
    const quizData = useSelector(state => state.quizStore.quizData?.results)

    useEffect(() => {
        dispatch(fetchQuizData({ categoriesDetails }))
    }, [])

    const isPageLoading = useSelector(state => state.quizStore.isPageLoading)

    if (isPageLoading) {
        return <Spinner />
    }

    const singleQuestion = quizData?.map(everyItem => everyItem.question)
    const singleAnswer = quizData?.map(everyItem => everyItem.correct_answer)
    const availableAnswers = quizData?.map(everyItem => everyItem.incorrect_answers)

    const handleSubmit = () => {
        if (isSelected) {
            setQuestionCount((previousValue) => previousValue + 1)
            setSelected(false);
            dispatch(saveUserAnswer(currentWord))
        } else alert("Please select an option")
    }

    const handleClickcorrect = (event) => {
        setCurrentWord(event)
        setSelected(true);
    }

    const handleClickIncorrect = (event) => {
        setSelected(true);
        setCurrentWord(event)
    }

    if (questionCount === 10) {
        return <ResultPage singleAnswer={singleAnswer} singleQuestion={singleQuestion} />
    }

    if (isPageLoading) {
        return <Spinner />
    }
    if (!quizData) {
        return <FileNotFound />;
    } else if (quizData.length === 0) {
        return <FileNotFound />
    }

    return (
        <div>
            <nav className="navbar navbar-expand-md bg-primary d-flex justify-content-between" >
                <Link to="/"><h3 className='text-light mx-4'>HOME</h3> </Link>
                <h1 className='text-light'>Answer Questions</h1>
                <h2 className='text-light mx-4'>✸</h2>
            </nav>
            <div className='m-5'>
                <div className='d-flex justify-content-center'>
                    <h2>Quiz Category -  {quizData && quizData[0]?.category}</h2>
                </div>
                <div className='d-flex justify-content-center'>
                    <h2>Quiz Level -  {quizData && quizData[0]?.difficulty}</h2>
                </div>
                <div className='container p-5 border shadow-lg p-3 mb-5 bg-body rounded'>
                    <h4>Question {questionCount + 1}/10</h4>
                    <div className="progress my-5">
                        <div
                            className="progress-bar "
                            role="progressbar"
                            aria-valuenow={10}
                            aria-valuemin={10}
                            aria-valuemax={10}
                            style={{ width: `${questionCount}0%` }}
                        />
                    </div>

                    <h2>{questionCount + 1}). {singleQuestion && singleQuestion[questionCount]}</h2>
                    <div className=' text-dark my-5' >
                        <h4 className={`answerClass m-2  ${(singleAnswer[questionCount] === currentWord) ? "bg-success text-light" : ""}`} key={singleAnswer && singleAnswer[questionCount]}
                            onClick={() => handleClickcorrect(singleAnswer[questionCount])}>
                            {singleAnswer && "1). " + singleAnswer[questionCount]}
                        </h4>
                        {availableAnswers ? (availableAnswers[questionCount] && availableAnswers[questionCount].map((everyIncorrectAnswer, index) => {
                            return <h4 key={index} className={`answerClass m-2  ${(everyIncorrectAnswer === currentWord) ? "bg-success text-light" : ""}`}
                                onClick={() => handleClickIncorrect(everyIncorrectAnswer)}>{index + 2 + "). " + everyIncorrectAnswer}</h4>
                        })) : <NotFound />}
                    </div>
                    <button className='btn btn-primary' onClick={() => handleSubmit()}>Submit</button>
                </div>
            </div>
        </div>
    )
}

export default QuestionsPage;
