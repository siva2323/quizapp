import React from 'react'
import { Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { saveCategoryNumber, saveDifficultyLevel } from '../features/quizSlice'
import { useSelector } from 'react-redux'

const DifficultyPage = () => {

    const categoryIndex = useSelector(state => state.quizStore.categoryIndex);
    const categoryName = useSelector(state => state.quizStore.categoryNames[categoryIndex])
    const categoryNumbers = useSelector(state => state.quizStore.categoryNumbers)
    const dispatch = useDispatch();

    dispatch(saveCategoryNumber(categoryNumbers[categoryIndex]))

    return (
        <div>
            <nav className="navbar navbar-expand-md bg-primary d-flex justify-content-between" >
                <Link to="/"><h3 className='text-light mx-4'>HOME</h3> </Link>
                <h1 className='text-light'>Choose Level</h1>
                <h3 className='text-light  mx-4'>✸</h3>
            </nav>

            <h3 className='text-center p-5'>Here we have three difficulty levels,go with each one and test your knowledge</h3>
            <h3 className='text-center m-3 text-primary '>Category - {categoryName}</h3>
            <h3 className='text-center'>Select the level of Quiz</h3>
            <div className='container d-flex mt-5 flex-wrap justify-content-center' >

                <Link to="easy" >
                    <div className="card text-white bg-primary m-3 " onClick={() => dispatch(saveDifficultyLevel("easy"))} >
                        <div className="card-body">
                            <h2 className="card-title">Easy</h2>
                            <p className="card-text">
                                This mode is Easy
                            </p>
                        </div>
                    </div>
                </Link>

                <Link to="medium" >
                    <div className="card text-white bg-primary m-3 " onClick={() => dispatch(saveDifficultyLevel("medium"))} >
                        <div className="card-body">
                            <h2 className="card-title">Medium</h2>
                            <p className="card-text">
                                This mode is Normal
                            </p>
                        </div>
                    </div>
                </Link>

                <Link to="hard" >
                    <div className="card text-white bg-primary m-3 " onClick={() => dispatch(saveDifficultyLevel("hard"))} >
                        <div className="card-body">
                            <h2 className="card-title">Hard</h2>
                            <p className="card-text">
                                This mode is Hard
                            </p>
                        </div>
                    </div>
                </Link>

            </div>
        </div>
    )
}

export default DifficultyPage