import React from 'react'
import { Link } from 'react-router-dom'

const NotFound = () => {
    return (
        <div>
            <nav className="navbar navbar-expand-md bg-secondary d-flex justify-content-center" >
                <h1 className='text-light'>404 Page Not Found</h1>
            </nav>
            <div className='container'>
                <Link to="/">
                    <button className='m-5 p-3 bg-primary text-light'>Go Home</button>
                </Link>
            </div>
        </div>
    )
}

export default NotFound;