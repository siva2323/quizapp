import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import { Provider } from 'react-redux';
import store from './app/store.jsx';
import { ErrorBoundary } from 'react-error-boundary';
import NotFound from './components/NotFound.jsx';

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <ErrorBoundary FallbackComponent={<NotFound />} >

            <Provider store={store} >
                <App />
            </Provider>

        </ErrorBoundary>
    </React.StrictMode>,
)
