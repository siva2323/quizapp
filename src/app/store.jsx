import { configureStore } from "@reduxjs/toolkit";
import quizSliceReducer from "../features/quizSlice";

const store = configureStore({
    reducer: {
        quizStore: quizSliceReducer,
    }
})

export default store;

