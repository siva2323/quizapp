import './App.css'
import HomePage from './components/HomePage'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import DifficultyPage from './components/DifficultyPage'
import NotFound from './components/NotFound'
import QuestionsPage from './components/QuestionsPage';
import { Suspense } from 'react';
import { useSelector } from 'react-redux';

const App = () => {

    const categoryName = useSelector(state => state.quizStore.categoryName)

    return (
        <BrowserRouter>
            <Suspense fallback={<div>LOADING....</div>} >
                <Routes>
                    <Route path="/" element={<HomePage />} />
                    <Route exact path={`/:${categoryName}`} element={<DifficultyPage />} />
                    <Route exact path="/:categoryName/:difficultyLevel" element={<QuestionsPage />} />
                    <Route exact path="*" element={<NotFound />} />
                </Routes>
            </Suspense>
        </BrowserRouter>
    )
}

export default App
